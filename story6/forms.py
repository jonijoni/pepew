from django import forms
# from django.utils.timezone import now
from .models import Acara, Peserta

class AcaraForm(forms.ModelForm):
    class Meta:
        model = Acara
        fields = ['name']

# class PesertaForm(forms.ModelForm):
#     class Meta:
#         model = Peserta
#         fields = 