# Generated by Django 3.0.3 on 2020-02-29 17:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story6', '0002_auto_20200301_0017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='acara',
            name='peserta',
            field=models.ManyToManyField(null=True, to='story6.Peserta'),
        ),
    ]
