# Generated by Django 3.0.3 on 2020-02-29 17:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story6', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='acara',
            name='peserta',
            field=models.ManyToManyField(to='story6.Peserta'),
        ),
    ]
