from django.shortcuts import get_object_or_404, render
from .forms import AcaraForm
from .models import Acara, Peserta
from django.db.models import Q
from django.http import HttpResponseRedirect
# Create your views here.
def index(request):
    acara = Acara.objects.all()
    if request.method == 'POST':
        name = request.POST['name']
        Acara.objects.create(name=name)
        return render(request, "story6.html", context={"form":AcaraForm(), "acaras":acara})
    return render(request, "story6.html", context={"form":AcaraForm(), "acaras":acara})

def detail(request, pk):
    acara = Acara.objects.get(id=pk)
    if request.method == 'POST':
        name = request.POST['name']
        peserta = Peserta.objects.create(name=name)
        print(peserta)
        print(name)
        acara.peserta.add(peserta)
        return render(request, "story6detail.html", context={"acara":acara, "form":AcaraForm()})

    return render(request, "story6detail.html", context={"acara":acara, "form":AcaraForm()})

def peserta_remove(request, pk):
    if request.method == 'POST':
        peserta = get_object_or_404(Peserta, pk=pk)
        peserta.delete()
        return HttpResponseRedirect('/story6/')
    return HttpResponseRedirect('/story6/')