from django.db import models
# from django.utils.timezone import now

class Peserta(models.Model):
    name = models.CharField(max_length=26, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name_plural = 'Pesertas'

    def __str__(self):
        return self.name
    objects = models.Manager()
    
class Acara(models.Model):
    name = models.CharField(max_length=20)
    peserta = models.ManyToManyField(Peserta, blank=True)

    class Meta:
        ordering = ('name',)
        verbose_name_plural = 'Acaras'

    def __str__(self):
        return self.name
    objects = models.Manager()