from django.urls import path, include
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.index),
    path('<int:pk>/', views.detail),
]
