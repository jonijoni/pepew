from django.shortcuts import render
from datetime import datetime, timedelta
from django.utils import timezone

# Create your views here.
def story3(request):
    return render(request, 'story3.html')

def cv(request):
    return render(request, 'cv.html')

# def get_location_timezone(obj):
#     """Returns the Location.timezone field from above"""

#     return obj.location.timezone

# def am(sum):
#     if sum > 12: 
#         return 'PM'
#     else: return "AM"

def time(request, tz):
    now = datetime.now()
    am = datetime.time(now)
    time = int(str(am).split(":")[0]) + int(tz)
    # pmoram = "AM"
    # if (time > 24): time = time % 24
    # elif (time <0) : time = 24 + time
    # if (time > 12): pmoram = "PM"
    return render(request, 'date.html', context= {'now' : datetime.now() + timedelta(hours=int(time))})