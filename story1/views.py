from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse, HttpResponseNotFound

def story1(request):
    return render(request, 'story1.html')