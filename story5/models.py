from django.db import models
# from .utils import generate_time
from django.utils.timezone import now

class Matkul(models.Model):
    name = models.CharField(max_length=20)
    dosen = models.CharField(max_length=15)
    sks = models.SmallIntegerField()
    desc = models.TextField()
    term = models.CharField(max_length=12)
    created_at = models.DateTimeField(default=now, editable=False)

    class Meta:
        ordering = ('name',)
        verbose_name_plural = 'Matkuls'

    def __str__(self):
        return self.name
    objects = models.Manager()

class Tugas(models.Model):
    name = models.CharField(max_length=20)
    deadline = models.DateTimeField()
    matkul = models.ForeignKey(Matkul, on_delete=models.CASCADE)

    class Meta:
        ordering = ('deadline',)
        verbose_name_plural = 'Tugasses'

    def __str__(self):
        return self.name
    objects = models.Manager()