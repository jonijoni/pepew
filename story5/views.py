from rest_framework import viewsets, status
# from rest_framework.filters import OrderingFilter
from django_filters import rest_framework
from rest_framework.response import Response
from rest_framework import filters
from django.shortcuts import get_object_or_404, redirect, render
from .models import Matkul, Tugas
from .forms import MatkulForm
from .serializers import MatkulSerializer
from django.http import HttpResponseRedirect, JsonResponse    
from django.db import models
from django.utils.timezone import now


# from django.http import JsonResponse

class MatkulViewSet(viewsets.ModelViewSet) :
    search_fields = ['name','sks', 'dosen', 'term','created_at']
    filter_backends = (
        rest_framework.DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter,)
    filter_fields = ('name', 'sks', 'dosen', 'term', 'created_at')
    serializer_class = MatkulSerializer
    queryset = Matkul.objects.all()

def matkulist(request): #the index view
    matkuls = Matkul.objects.all() #quering all todos with the object manager
    if request.method == 'POST':
        name = request.POST['name']
        dosen = request.POST['dosen']
        sks = request.POST['sks']
        desc = request.POST['desc']
        term = request.POST['term']

        # matkul = Matkul(name=name, dosen=dosen,
        #                 sks=sks, desc=desc, term=term)
        Matkul.objects.create(name=name, dosen=dosen,
                        sks=sks, desc=desc, term=term)
        # serializers = MatkulSerializer(matkul, many=True)
        form = MatkulForm()
        return render(request, "matkulist.html", context={"matkuls": matkuls, 'form':form})
    else:
        # return JsonResponse(data=None, status=405)
    # if request.method == "POST": #checking if the request method is a POST
    #     data_serializer = MatkulSerializer(data=request.data)
    #     if data_serializer.is_valid():
    #         data_serializer.save()
        # if "taskAdd" in request.POST: #checking if there is a request to add a todo
        #     title = request.POST["description"] #title
        #     date = str(request.POST["date"]) #date
        #     category = request.POST["category_select"] #category
        #     content = title + " -- " + date + " " + category #content
        #     Todo = TodoList(title=title, content=content, due_date=date, category=Category.objects.get(name=category))
        #     Todo.save() #saving the todo 
        #     return redirect("/") #reloading the page
        # if "taskDelete" in request.POST: #checking if there is a request to delete a todo
        #     checkedlist = request.POST["checkedbox"] #checked todos to be deleted
        #     for todo_id in checkedlist:
        #         todo = TodoList.objects.get(id=int(todo_id)) #getting todo id
        #         todo.delete() #deleting todo
        form = MatkulForm()
        return render(request, "matkulist.html", context={"matkuls": matkuls, 'form': form })

def matkul(request, pk): #the index view
    matkul = Matkul.objects.get(id=pk)
    tugas = Tugas.objects.filter(matkul=matkul)
    color = []
    for m in tugas:
        if((m.deadline - now()).total_seconds() < 0 ):
            color.append("bg-secondary")
        elif((m.deadline - now()).total_seconds() < 86400 ):
            color.append("bg-danger")
        else:
            color.append("bg-primary")
    counter = 0
    tutu = []
    for t in tugas:
        print(t)
        l = (t, color[counter])
        tutu.append(l)
        print(l)
        counter+=1
    # for t in tugas:
    #     print (t
    return render(request, "matkul.html", context={"matkul": matkul, "tugasses":tutu, })

def matkul_remove(request, pk):
    matkul = get_object_or_404(Matkul, pk=pk)
    matkul.delete()
    return HttpResponseRedirect('/matkul')