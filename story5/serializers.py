from rest_framework import serializers
from .models import Matkul, Tugas
from django.utils.timezone import now
from datetime import datetime

class MatkulSerializer(serializers.ModelSerializer):
    # subtotal = serializers.SerializerMethodField()
    
    class Meta:
        model = Matkul
        fields = ['id', 'name', 'dosen', 'sks', 'desc', 'term', 'created_at']


    # def get_subtotal(self, obj):
    #         return obj.price * obj.quantity

class TugasSerializer(serializers.ModelSerializer):
    color = serializers.SerializerMethodField()
    class Meta:
        model = Tugas
        field = ['deadline','matkul']

    def get_color(self, obj):
        sec = obj.deadline.timestamp() - now.timestamp()
        if (sec < 0):
            return "gray"
        elif (sec  <= 86400 * 3) :
            return "red"
        else:
            return "blue"