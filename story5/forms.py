from django import forms
# from django.utils.timezone import now
from .models import Matkul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ['id', 'name', 'dosen', 'sks', 'desc', 'term']