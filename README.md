# PTI Shopping Cart
https://joni-pti.herokuapp.com/api/
# Endpoints
# /item
### POST request
{
	"name" : "My Favorite Pillow",
	"price" : "10000",
	"quantity" : "69",
}
### GET Request
/?search=<something>
/?name=<itemname>&price=<price>

# /item/{id}
